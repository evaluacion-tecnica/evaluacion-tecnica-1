/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test1;

import java.util.Date;

/**
 *
 * @author Diego
 */
public class Deuda {
    private String idCliente;
    private String nombreCliente;
    private String Correo;
    private int montoDeuda;
    private String idDeuda;
    private String fecha;
    private static Deuda miDeuda;

    public Deuda(String idCliente, String nombreCliente, String Correo, int montoDeuda, String idDeuda, String fecha) {
        this.idCliente = idCliente;
        this.nombreCliente = nombreCliente;
        this.Correo = Correo;
        this.montoDeuda = montoDeuda;
        this.idDeuda = idDeuda;
        this.fecha = fecha;
    }

    public Deuda() {
    }
    
    public static Deuda getDeuda(){
        if (miDeuda == null) {
            miDeuda = new Deuda();
        }
        return miDeuda;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getMontoDeuda() {
        return montoDeuda;
    }

    public void setMontoDeuda(int montoDeuda) {
        this.montoDeuda = montoDeuda;
    }

    public String getIdDeuda() {
        return idDeuda;
    }

    public void setIdDeuda(String idDeuda) {
        this.idDeuda = idDeuda;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "deudas{" + "idCliente=" + idCliente + ", nombreCliente=" + nombreCliente + ", Correo=" + Correo + ", montoDeuda=" + montoDeuda + ", idDeuda=" + idDeuda + ", fecha=" + fecha + '}';
    }
    
    
}
