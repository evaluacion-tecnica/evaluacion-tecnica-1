/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.test1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;

/**
 *
 * @author Diego
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        File archivo = new File("D:\\Descargas\\deudas.txt");
        FileReader fr = new FileReader(archivo); 
        List<Deuda> deudas = LeerArchivo(fr);
        subirDeuda(deudas);
    }

    public static List<Deuda> LeerArchivo(FileReader fr) {
        List<Deuda> deudas = new ArrayList<>();

        try {
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null && line.length() > 0) {
                String[] datos = line.split(";");

                Deuda deuda = insertarDatos(datos);

                deudas.add(deuda);

                line = br.readLine();
            }
        } catch (IOException e) {
        }
        return deudas;
    }

    public static Deuda insertarDatos(String[] datos) {
        String idCliente = null;
        String nombreCliente = null;
        String Correo = null;
        int montoDeuda = 0;
        String idDeuda = null;
        String fecha = null;
                
        if (validadorRut(datos[0])) {
            idCliente = datos[0];
        }
        if (validadorNombre(datos[1])) {
            nombreCliente = datos[1];
        }
        if (validadorCorreo(datos[2])) {
            Correo = datos[2];
        }
        if (validadorRut(datos[3])) {
            montoDeuda = Integer.parseInt(datos[3]);
        }
        if (validadorIdDeuda(datos[4])) {
            idDeuda = datos[4];
        }
        if (validadorFecha(datos[5])) {
            fecha = datos[5];
        }
        return new Deuda(idCliente, nombreCliente, Correo, montoDeuda, idDeuda, fecha);
    }

    /////////////////////////////-Validadores-//////////////////////////////////
    public static boolean validadorRut(String rut) {
        Pattern pattern = Pattern.compile("^[0-9]+-[0-9kK]{1}$");
        Matcher matcher = pattern.matcher(rut);
        if (matcher.matches() == false && rut.length() > 15) {
            return false;
        }
        String[] stringRut = rut.split("-");
        return true;
    }

    public static boolean validadorNombre(String nombre) {
        if (nombre.length() > 60) {
            return false;
        }
        return true;
    }

    public static boolean validadorCorreo(String correo) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        									+"[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher matcher = pattern.matcher(correo);
        if (matcher.find() == false && correo.length() > 60) {
            return true;
        }
        return true;
    }

    public static boolean validadorMontoDeuda(String monto){
        if (monto.length() > 20) {
            return true;
        }
        return true;
    }
    public static boolean validadorIdDeuda(String idDeuda) {
        if (idDeuda.length() > 15) {
            return false;
        }
        return true;
    }
    public static boolean validadorFecha(String text) {
        SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy");
        df.setLenient(false);
        try {
            df.parse(text);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }
    
    /////////////////////////Subidadearchivo////////////////////////////////
    public static void subirDeuda(List<Deuda> vo) {
        Conectar conec = new Conectar();
        String sql = "INSERT INTO deuda (idClient, nameClient, mail, amountDebt, idDebt, debtDate)\n" + 
                "VALUES (?,?,?,?,?,?);";
        PreparedStatement ps = null;
        try {
            for(Deuda deuda : vo ) {
            	ps = conec.getConexion().prepareStatement(sql);
                ps.setString(1, deuda.getIdCliente());
                ps.setString(2, deuda.getNombreCliente());
                ps.setString(3, deuda.getCorreo());
                ps.setInt(4, deuda.getMontoDeuda());
                ps.setString(5, deuda.getIdDeuda());
                ps.setString(6, deuda.getFecha());
                ps.executeUpdate();
            }
            JOptionPane.showMessageDialog(null, "Archivo subido");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "No se pudo subir el archivo");
        } 
    }
}
